from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Date, Float

Base = declarative_base()


class Vendedor(Base):
    __tablename__ = 'vendedores'

    id = Column(Integer, primary_key=True)
    nome = Column(String)

    def __repr__(self):
        return f'Venderdor(id="{self.id}", nome="{self.nome}"'


class Venda(Base):
    __tablename__ = 'vendas'

    id = Column(Integer, primary_key=True)
    data_venda = Column(Date)
    valor = Column(Float)
    id_vendedor = Column(Integer)

    def __repr__(self):
        return f'Venda(id="{self.id}", data_venda="{self.data_venda}, valor={self.valor}, id_vendedor={self.id_vendedor}"'