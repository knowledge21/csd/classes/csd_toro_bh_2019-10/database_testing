from .models import Venda, Vendedor


class VendaDAO:

    def __init__(self, session):
        self.session = session

    def buscarTotalDeVendasPorVendedorEAno(self, id_vendedor: int, ano: int) -> float:
        vendas = self.session.query(Venda).filter(Venda.id_vendedor == id_vendedor).all()

        total_vendas = 0
        for venda in vendas:
            total_vendas += venda.valor

        return total_vendas
