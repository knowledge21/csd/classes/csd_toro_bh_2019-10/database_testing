from sqlalchemy.orm import sessionmaker

from sistema_vendas.dal import VendaDAO
from sistema_vendas.models import Base, Venda, Vendedor
from tests.database_test.database_test import DatabaseTestCase


class TestVendaDAO(DatabaseTestCase):

    def test_vendedor_inexistente(self):
        ano = 2017
        id_vendedor = 999
        total_de_vendas_esperado = 0

        valor_total_retornado = VendaDAO(self.get_session()).buscarTotalDeVendasPorVendedorEAno(id_vendedor, ano)

        self.assertEqual(total_de_vendas_esperado, valor_total_retornado)

    def teste_duas_vendas_no_mesmo_ano(self):
        ano = 2017
        id_vendedor = 1
        total_de_vendas_esperado = 142

        valor_total_retornado = VendaDAO(self.get_session()).buscarTotalDeVendasPorVendedorEAno(id_vendedor, ano)

        self.assertEqual(total_de_vendas_esperado, valor_total_retornado)

    def teste_uma_venda_ano(self):
        ano = 2016
        id_vendedor = 2
        total_de_vendas_esperado = 55.59

        valor_total_retornado = VendaDAO(self.get_session()).buscarTotalDeVendasPorVendedorEAno(id_vendedor, ano)

        self.assertEqual(total_de_vendas_esperado, valor_total_retornado)

    def teste_duas_vendas_vendedores_diferentes_ano(self):
        ano = 2016
        id_vendedor = 3
        total_de_vendas_esperado = 200

        valor_total_retornado = VendaDAO(self.get_session()).buscarTotalDeVendasPorVendedorEAno(id_vendedor, ano)

        self.assertEqual(total_de_vendas_esperado, valor_total_retornado)
