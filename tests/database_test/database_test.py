import json
from datetime import datetime
from unittest import TestCase

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from sistema_vendas.models import Base, Venda, Vendedor


class DatabaseTestCase(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.engine = create_engine('sqlite:///:memory:')

    def setUp(self) -> None:
        Base.metadata.drop_all(self.engine)
        Base.metadata.create_all(self.engine)
        self.load_database('dados.json')

    def get_session(self):
        return sessionmaker(bind=self.engine)()

    def load_database(self, data_file):
        session = self.get_session()
        with open(data_file) as json_file:
            data = json.load(json_file)
            for vendedor in data['vendedor']:
                session.add(Vendedor(**vendedor))
            for venda in data['venda']:
                session.add(Venda(id=venda['id'], data_venda=datetime.strptime(venda['data_venda'], "%Y-%m-%d"),
                                  valor=venda['valor'], id_vendedor=venda['id_vendedor']))
            session.commit()